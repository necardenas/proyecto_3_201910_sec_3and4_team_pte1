package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import controller.saxparser.MapParser;
import model.data_structures.LinearProbingHashST;
import model.data_structures.UndirectedGraph;
import model.data_structures.Vertice;
import model.vo.InfoEdge;
import model.vo.InfoVertice;
import model.vo.VOMovingViolations;

public class Load {

	private String filePath = "data/XMLs/map.xml";

	private LinearProbingHashST<String, VOMovingViolations> moviongViolations;

	private UndirectedGraph<InfoEdge, InfoVertice> map;

	double latitudMin = Double.MAX_VALUE, longitudMin = 0, latitudMax = 0, longitudMax = Integer.MIN_VALUE;

	public Load() {
		map = new UndirectedGraph<>();
		readVOMovingViolations();
		loadJSON();
		loadMap();

	}

	private void loadMap() {

	      try {
	         File inputFile = new File(filePath);
	         SAXParserFactory factory = SAXParserFactory.newInstance();
	         SAXParser saxParser = factory.newSAXParser();
	         MapParser userhandler = new MapParser(map);
	         saxParser.parse(inputFile, userhandler);     
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	   
	}

	public LinearProbingHashST<String, VOMovingViolations> getMoviongViolations() {
		return moviongViolations;
	}

	public UndirectedGraph<InfoEdge, InfoVertice> getMap() {
		return map;
	}

	public double getLatitudMin() {
		return latitudMin;
	}

	public double getLongitudMin() {
		return longitudMin;
	}

	public double getLatitudMax() {
		return latitudMax;
	}

	public double getLongitudMax() {
		return longitudMax;
	}

	public void readVOMovingViolations() {
		CsvParserSettings settings = new CsvParserSettings();
		settings.getFormat().setLineSeparator("\n");
		settings.getFormat().setDelimiter(";");

		CsvParser parser = new CsvParser(settings);
		int numInfracs = 0;

		moviongViolations = new LinearProbingHashST<>();
		double latitudeMin = Double.MAX_VALUE, longitudeMin = 0, latitudeMax = 0, longitudeMax = Integer.MIN_VALUE;
		try {
			for (int i = 1; i < 13; i++) {

				String cPath = "data/Datos Proyecto 3/" + (i) + "_wgs84.csv";
				int tempInfrac = 0;
				parser.beginParsing(new FileReader(cPath));
				parser.parseNext();
				String[] row = null;

				while ((row = parser.parseNext()) != null) {
					String in1 = row[1].toString();
					Integer objectID = Integer.parseInt(in1);

					String location = row[3].toString();

					int addressID = 0;
					if (row[4] != null) {
						String in2 = row[4].toString();
						addressID = Integer.parseInt(in2);
					}

					double streetSegID = 0;
					if (row[5] != null) {
						String in3 = row[5].toString();
						streetSegID = Double.parseDouble(in3);
					}

					String ticketType = row[8].toString();

					String in4 = row[9].toString();
					Integer fineamt = Integer.parseInt(in4);

					String in5 = row[10].toString();
					double totalPaid = Double.parseDouble(in5);

					String in6 = row[11].toString();
					Integer penalty1 = Integer.parseInt(in6);

					String accidentIndicator = row[13].toString();

					String ticketIssueDate = "";
					if (row[14] != null)
						ticketIssueDate = row[14].toString();

					String violationCode = row[15].toString();

					String violationDesc = "";
					if (row[16] != null)
						violationDesc = row[16].toString();

					String in7 = row[18].toString();
					Double latitude = Double.parseDouble(in7.replace(',', '.'));

					String in8 = row[19].toString();
					Double longitude = Double.parseDouble(in8.replace(',', '.'));

					VOMovingViolations vomv = new VOMovingViolations(objectID, location, addressID, streetSegID,
							ticketType, fineamt, totalPaid, penalty1, accidentIndicator, ticketIssueDate, violationCode,
							violationDesc, latitude, longitude);
					// Se agrega la infraccion a la estructura(s) de datos
					moviongViolations.put("" + objectID, vomv);
					tempInfrac++;

					if (latitude > latitudeMax)
						latitudeMax = latitude;

					if (longitude > longitudeMax)
						longitudeMax = longitude;

					if (latitude < latitudeMin)
						latitudeMin = latitude;

					if (longitude < longitudeMin)
						longitudeMin = longitude;
				}
				numInfracs += tempInfrac;
				System.out.println("En el mes " + (i) + " se encontraron " + tempInfrac + " infracciones");
				parser.stopParsing();

			}
			System.out.println("Se encontraron " + numInfracs + " infracciones en el anio.");
			System.out.println();
			System.out.println("Minimax: " + "(" + latitudeMin + ", " + longitudeMin + "), " + "(" + latitudeMax + ", "
					+ longitudeMax + ")");
		}

		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println();
			System.out.println("No se encontr� el archivo");
			System.out.println();
		}
	}

	/**
	 * Cargar el Grafo No Dirigido de la malla vial: Downtown o Ciudad Completa
	 * 
	 * @param rutaArchivo
	 */

	public void loadJSON() {
		// JSON parser object to parse read file
		JsonParser jsonParser = new JsonParser();
		File file = new File("data/finalGraph.json");

		try (FileReader reader = new FileReader(file)) {
			BufferedReader br = new BufferedReader(reader);
			br.readLine();
			String line = br.readLine();
			StringBuilder txt = new StringBuilder();
			int i =0;
			while (line != null) {
				txt.append(line);
				if (line.contains("}") || line.contains("},")) {
					if (txt.toString().endsWith(",")) {
						String t= txt.toString();
						txt = new StringBuilder(t.substring(0, t.length() - 1));
					}
					com.google.gson.JsonObject obj = (JsonObject) jsonParser.parse(txt.toString());
					loadObject(obj);
					txt = new StringBuilder();
				}
				line = br.readLine();
				
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadObject(com.google.gson.JsonObject o) {
		String vertexId = o.get("id").getAsString();
		double latitude = o.get("lat").getAsDouble();
		double longitude = o.get("lon").getAsDouble();

		InfoVertice infoVertice = new InfoVertice();
		infoVertice.setId(vertexId);
		infoVertice.setLatitude(latitude);
		infoVertice.setLongitude(longitude);
		JsonArray jsonInfractions = o.get("infractions").getAsJsonArray();
		for (JsonElement jsonElement : jsonInfractions) {
			String temp = jsonElement.getAsString();
			VOMovingViolations vo = moviongViolations.get(temp);
			infoVertice.addMoviongViolation(vo);
		}

		Vertice<InfoVertice> vertice = new Vertice<InfoVertice>(vertexId, infoVertice);
		JsonArray jsonList = o.get("adj").getAsJsonArray();
		for (JsonElement jsonElement : jsonList) {
			String temp = jsonElement.getAsString();
			vertice.addAdj(temp);
		}
		map.addVertice(vertexId, vertice);
	}


}
