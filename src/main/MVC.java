package main;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import com.teamdev.jxmaps.MapViewOptions;

import controller.Controller;
import map.StreetViewMap;

public class MVC {

	public static void main(String[] args) 
	{
		
		MapViewOptions mvo = new MapViewOptions();
		mvo.importPlaces();
		mvo.setApiKey("7a1y3jpxlg70k7covavndushu12jznd6puexhj0sc1q1truqbyyz5afwnqpkmtidk1kkspfoc9z0zt25");

		StreetViewMap sample = new StreetViewMap();

        JFrame frame = new JFrame("Street View");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(sample, BorderLayout.CENTER);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
		
		Controller controler = new Controller();
		controler.run();
	}

}
