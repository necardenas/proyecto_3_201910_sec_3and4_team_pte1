package model.data_structures;

import java.util.Iterator;
import java.util.List;

import model.vo.*;

/**
*  The {@code LazyPrimMST} class represents a data type for computing a
*  <em>minimum spanning tree</em> in an edge-weighted graph.
*  The edge weights can be positive, zero, or negative and need not
*  be distinct. If the graph is not connected, it computes a <em>minimum
*  spanning forest</em>, which is the union of minimum spanning trees
*  in each connected component. The {@code weight()} method returns the 
*  weight of a minimum spanning tree and the {@code edges()} method
*  returns its edges.
*  <p>
*  This implementation uses a lazy version of <em>Prim's algorithm</em>
*  with a binary heap of edges.
*  The constructor takes time proportional to <em>E</em> log <em>E</em>
*  and extra space (not including the graph) proportional to <em>E</em>,
*  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
*  Afterwards, the {@code weight()} method takes constant time
*  and the {@code edges()} method takes time proportional to <em>V</em>.
*  <p>
*  For additional documentation,
*  see <a href="https://algs4.cs.princeton.edu/43mst">Section 4.3</a> of
*  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
*  For alternate implementations, see {@link PrimMST}, {@link KruskalMST},
*  and {@link BoruvkaMST}.
*
*  @author Robert Sedgewick
*  @author Kevin Wayne
*/
public class PrimMST {
   private static final double FLOATING_POINT_EPSILON = 1E-12;

   private double weight;       // total weight of MST
   private Queue<Edge> mst;     // edges in the MST
   private SeparateChainingHashST<String, Boolean> marked;    // marked[v] = true if v on tree
   private MinPQ<Edge> pq;      // edges with one endpoint in tree
   private SeparateChainingHashST<Dupla, Edge> edgeHash;

   /**
    * Compute a minimum spanning tree (or forest) of an edge-weighted graph.
    * @param G the edge-weighted graph
    */
   public PrimMST(UndirectedGraph G) {
       mst = new Queue<Edge>();
       pq = new MinPQ<Edge>();
       edgeHash = G.getEdgesHash();
       marked = new SeparateChainingHashST<>();
       Iterator<String> it = G.getVertices().iterator();
       while (it.hasNext())
       	marked.put(it.next(), false);
       
       it = G.getVertices().iterator();
       while (it.hasNext())
       {
       	String key = it.next();
       	if (!marked.get(key))
       		prim(G, key);
       }
   }

   // run Prim's algorithm
   private void prim(UndirectedGraph G, String s) {
       scan(G, s);
       while (!pq.isEmpty()) {                        // better to stop when mst has V-1 edges
           Edge e = pq.delMin();                      // smallest edge on pq
           Vertice<InfoVertice> v = e.getVertice1(), w = e.getVertice2();        // two endpoints
           assert marked.get(v.getKey()) || marked.get(w.getKey());
           if (marked.get(v.getKey()) && marked.get(w.getKey())) continue;      // lazy, both v and w already scanned
           mst.enqueue(e);                            // add e to MST
           weight += getCost(e);
           if (!marked.get(v.getKey())) scan(G, v.getKey());               // v becomes part of tree
           if (!marked.get(w.getKey())) scan(G, w.getKey());               // w becomes part of tree
       }
   }

   // add all edges e incident to v onto pq if the other endpoint has not yet been scanned
   private void scan(UndirectedGraph G, String v) {
       assert !marked.get(v);
       marked.put(v, true);
       for (Vertice w : ((List<Vertice>) G.getVertice(v).getAdj()) )
       {
       	Dupla d = new Dupla(G.getVertice(v), w);
           if (!marked.get(w.getKey())) pq.insert(edgeHash.get(d));
       }
   }
       
   /**
    * Returns the edges in a minimum spanning tree (or forest).
    * @return the edges in a minimum spanning tree (or forest) as
    *    an iterable of edges
    */
   public Iterable<Edge> edges() {
       return mst;
   }

   /**
    * Returns the sum of the edge weights in a minimum spanning tree (or forest).
    * @return the sum of the edge weights in a minimum spanning tree (or forest)
    */
   public double weight() {
       return weight;
   }
   
   private double getCost(Edge<Object, InfoVertice> e)
   {
	   double lat1 = e.getVertice1().getInfo().getLatitude();
	   double long1 = e.getVertice1().getInfo().getLongitude();
	   double lat2 = e.getVertice2().getInfo().getLatitude();
	   double long2 = e.getVertice2().getInfo().getLongitude();
	   double r = Haversine.distance(lat1, long1, lat2, long2);
	   return r;
   }
}
