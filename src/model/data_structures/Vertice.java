package model.data_structures;

import java.util.ArrayList;

public class Vertice<V> {

	private String key; 
	
	private V info;

	private ArrayList<String> adj;
	
	public Vertice(String key, V info) {
		this.key  =  key;
		this.info = info;
		adj = new ArrayList<>();
	}
	
	public void addAdj(String vertex) {
		adj.add(vertex);
	}

	
	public String getKey() {
		return key;
	}

	public V getInfo() {
		return info;
	}

	public void setInfo(V info) {
		this.info = info;
	}

	public ArrayList<String> getAdj() {
		return adj;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adj == null) ? 0 : adj.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertice other = (Vertice) obj;
		if (adj == null) {
			if (other.adj != null)
				return false;
		} else if (!adj.equals(other.adj))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}

}

