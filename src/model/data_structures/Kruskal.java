package model.data_structures;

import java.util.ArrayList;
import java.util.List;

import model.comparators.Comparator2C;
import model.util.Sort;

public class Kruskal {
	public List<Edge> kruskalAlgorithm(List<Edge> edges, int nodeCount) 
	{
		DisjointSet ds = new DisjointSet(nodeCount);
		List<Edge> spanningTree = new ArrayList<Edge>();
		
		Edge[] array = new Edge[edges.size()];
		int i = 0;
		for (Edge e: edges)
		{
			array[i] = e;
			i++;
		}
		
		Comparator2C comparator = new Comparator2C();
		Sort.quickSort3(array, 0, array.length-1, comparator);
		i = 0;
		while (i != edges.size() && spanningTree.size() != nodeCount - 1) {
			Edge e = edges.get(i);
			if(ds.find(
					//e.getVertice1()
					0) != ds.find(
							//e.getVertice2()
							1)){
				spanningTree.add(e);
				ds.union(
						//e.getVertice1(), e.getVertice2()
						0,1);
			}
			i++;
		}
		return spanningTree;
	}
	
	public int weight(){
		return 0;
	}

	/**
	 * Implementation of the disjoint set structure
	 * @author Pavel Micka
	 */
	class DisjointSet {

		private Node[] nodes;

		public DisjointSet(int nodeCount) {
			nodes = new Node[nodeCount];
			for (int i = 0; i < nodeCount; i++) {
				nodes[i] = new Node();
				nodes[i].id = i;
			}
		}

		/**
		 * Unions sets, which contain vertices "a" and "b"
		 * Union is always performed as merging "B" into "A"
		 * @param a number of a node "a"
		 * @param b number of a node "b"
		 * @return number of a representative of the merged component
		 */
		public int union(int a, int b) {
			Node repA = nodes[find(a)];
			Node repB = nodes[find(b)];

			repB.parent = repA;
			return repA.id;
		}

		/**
		 * Returns representative of the compoment which contains the given node
		 * @param a number of a node, whose representative we are looking for
		 * @return number of the representative
		 */
		public int find(int a) {
			Node n = nodes[a];
			int jumps = 0;
			while (n.parent != null) {
				n = n.parent;
				jumps++;
			}
			if (jumps > 1) repair(a, n.id);
			return n.id;
		}

		/**
		 * Performs compression of the path
	155.
		 * @param a
		 * @param rootId
		 */
		private void repair(int a, int rootId) {
			Node curr = nodes[a];
			while (curr.id != rootId) {
				Node tmp = curr.parent;
				curr.parent = nodes[rootId];
				curr = tmp;
			}
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < nodes.length; i++) {
				builder.append(find(i) + " ");
			}
			return builder.toString();
		}
	}
	
	private static class Node {
		Node parent;
		int id;
	}
}
