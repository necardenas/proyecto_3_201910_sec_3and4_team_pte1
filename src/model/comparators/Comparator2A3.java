package model.comparators;

import java.util.Comparator;

import model.data_structures.*;
import model.vo.*;

public class Comparator2A3 implements Comparator<Queue<InfoVertice>>{

	public int compare(Queue<InfoVertice> q1, Queue<InfoVertice> q2) {
		
		if (q1.size() > q2.size())
			return 1;
		else if (q1.size() < q2.size())
			return -1;
		else
			return 0;
		
	}

}
