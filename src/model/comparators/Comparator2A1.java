package model.comparators;

import java.util.Comparator;
import model.vo.InfoVertice;

public class Comparator2A1 implements Comparator<InfoVertice> {

	public int compare(InfoVertice i1, InfoVertice i2) {
		if (i1.getQueue().size() > i2.getQueue().size())
			return 1;
		else if (i1.getQueue().size() < i2.getQueue().size())
			return -1;
		else
			return 0;
	}
	
}
