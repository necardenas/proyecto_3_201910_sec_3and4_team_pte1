package model.comparators;

import java.util.Comparator;
import model.vo.*;
import model.data_structures.*;

public class Comparator2C implements Comparator<Edge<Object, InfoVertice>>{

	@Override
	public int compare(Edge<Object, InfoVertice> o1, Edge<Object, InfoVertice> o2) {
		double lat1 = o1.getVertice1().getInfo().getLatitude();
		double long1 = o1.getVertice1().getInfo().getLongitude();
		double lat2 = o1.getVertice2().getInfo().getLatitude();
		double long2 = o1.getVertice2().getInfo().getLongitude();
		double d1 = Haversine.distance(lat1, long1, lat2, long2);
		
		double laat1 = o2.getVertice1().getInfo().getLatitude();
		double loong1 = o2.getVertice1().getInfo().getLongitude();
		double laat2 = o2.getVertice2().getInfo().getLatitude();
		double loong2 = o2.getVertice2().getInfo().getLongitude();
		double d2 = Haversine.distance(laat1, loong1, laat2, loong2);
		
		if (d1 > d2)
			return 1;
		else if (d1 < d2)
			return -1;
		else
			return 0;
	}

}
